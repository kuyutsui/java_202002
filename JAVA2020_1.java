import java.util.*;

public class JAVA2020_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		courseExecrise ce = new courseExecrise();
		Scanner input = new Scanner(System.in);
		String[] breakfast= {"Sausage","Eggs","Beans","Bacon","Tomatoes","Mushrooms"};
		String[] palindromic= {"Sausage","Eggs","Beans","Beans","Eggs","Sausage"};
		char[] letters= {'a','a','a','a','b','c','c','a','a','d','e','e','e','e'};
		String str ="I never saw a purpl cow";
		
		//Odd Number Less Then 20
		System.out.println("1. Odd Number Less Then 20");
		ce.oddNum();
		System.out.println();
		
		//Square Numbers Up To 100
		System.out.println("2. Square Numbers Up To 100");
		ce.squares();
		System.out.println();
		
		//Power of 2
		System.out.println("3. Power of 2");
		System.out.print("Please Enter a Number: ");
		int n = Integer.parseInt(input.nextLine());
		ce.powOf2(n);
		System.out.println();
		
		//Leap Year
		System.out.println("4. Leap Year");
		System.out.print("Please Enter a Year: ");
		int v_year = Integer.parseInt(input.nextLine());
		if (ce.leapYear(v_year)) {
			System.out.printf("%d is a leap year.\n", v_year);
		}
		else {
			System.out.printf("%d is not a leap year.\n", v_year);
		}
		System.out.println();
		
		//Last Element of Array
		System.out.println("5. Last Element of Array");
		System.out.print("Input String: " + Arrays.toString(breakfast) + "\n");
		System.out.print("Last Element of Array: " + ce.lastElement(breakfast) + "\n");
		
		//Reverse an Array
		System.out.println("6. Reverse an Array");
		System.out.print("Input String: " + Arrays.toString(breakfast) + "\n");
		System.out.print("Reverse: ");
		String[] rBreakfast = ce.reverseArray(breakfast);
		for(int x = 0; x < rBreakfast.length; x++) {
			System.out.println(rBreakfast[x]);
		}
		System.out.println();
		
		//Palindromic Array
		System.out.println("7. Palindromic Array");
		System.out.print("Input String: " + Arrays.toString(breakfast) + ".\n Palindromic: ");
		if(ce.palindromic(breakfast)) {
			System.out.println("True");
		}
		else {
			System.out.println("Flase");
		}
		System.out.print("Input String: " + Arrays.toString(palindromic) + ".\n Palindromic: ");
		if(ce.palindromic(palindromic)) {
			System.out.println("True");
		}
		else {
			System.out.println("Flase");
		}
		
		//Pack Duplicates
		System.out.println("8. Pack Duplicates");
		System.out.print("Input String: " + Arrays.toString(letters) + ".\nPack Duplicates: " + ce.packDup(letters) + "\n");
		
		//Count Words
		System.out.println("9. Count Words");
		System.out.print("Input String: " + str + ".\n Count " + ce.countWords(str) + " Word(s)\n");
		
		//Count Primes
		System.out.println("10. Count Primes");
		System.out.print("Please Enter a Number: ");
		int inum = Integer.parseInt(input.nextLine());
		System.out.printf("Less Then %d HAVE %d The Prime(s)\n",inum ,ce.countPrimes(inum).size());

	}

}
