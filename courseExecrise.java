import java.util.*;

public class courseExecrise {

	public courseExecrise() {
		// TODO Auto-generated constructor stub
	}
	
	public void oddNum() {
		int lineCount = 1;
		System.out.printf("%2d: oddNum()\n", lineCount);
		
		for(int x = 1; x <= 20; x++) 
		{
			if(Math.floorMod(x, 2) != 0)
			System.out.printf("%2d: %4d\n", ++lineCount, x);
		}
			
	}

	public void squares() {
		int lineCount = 1;
		System.out.printf("%2d: squares()\n", lineCount);
		
		for(int x = 1; Math.pow(x, 2) <= 100; x++) {
			double pow = Math.pow(x, 2);
			System.out.printf("%2d: %.0f\n", ++lineCount, pow);
		}
	}
	
	public void powOf2(int n) {
		int lineCount = 1;
		System.out.printf("%2d: powOf2(%d)\n", lineCount, n);
		
		for(int x = 1; x <= n; x++) {
			double pow = Math.pow(2, x);
			System.out.printf("%2d: %.0f\n", ++lineCount, pow);
		}
	}
	
	public boolean leapYear(int n) {
		if((Math.floorMod(n, 4) == 0 && Math.floorMod(n, 100) != 0) ||
		  (Math.floorMod(n, 400) == 0))
		  return true;
		else
		  return false;
	}
	
	public String lastElement(String[] s) {
		return s[s.length-1];
	}
	
	public String[] reverseArray(String[] s) {
		String[] r = new String[s.length];
		
		for(int x = 0; x < s.length; x++) {
			r[x] = s[s.length - x - 1];
		}
		
		return r;
	}
	
	public boolean palindromic(String[] s) {
		if (Arrays.equals(s, reverseArray(s)))
		 return true;
		else
		 return false;
	}
	
	public String packDup(char[] c){
		HashMap<Character, String> map = new HashMap<Character, String>();
		String result = new String();
		
		for(int x = 0; x < c.length; x++) {
			if(map.containsKey(c[x])) {
				String dup = map.get(c[x]) + c[x];
				map.put(c[x], dup);
			}
			else
				map.put(c[x], Character.toString(c[x]));
		}
		
		for(Character key: map.keySet()) {
			String value = map.get(key);
			result += value + " ";
		}
		
		return result;
	}
	
	public int countWords(String s) {
		StringTokenizer str = new StringTokenizer(s);
		int count = 0;
		
		for(int i = 1; str.hasMoreTokens(); i++) {
			count++;
			str.nextToken();
		}
		return count;
	}
	
	public ArrayList<Integer> countPrimes(int inum){
		ArrayList<Integer> prime = new ArrayList<Integer>();
		
		for(int x = 2; x <= inum; x++) {
			boolean is_prime = true;
			
			for(int i = 2; i < x; i++) {
				if(Math.floorMod(x, i) == 0) {
					is_prime = false;
					break;
				}
			}
			if (is_prime) {
				prime.add(x);
			}
		}
		return prime;
	}
}
