package FileIO;

import java.io.*;
import java.math.*;
import java.util.*;

public class person {
	int height;
	int weight;
	String eName;
	String cName;
	int ext;
	String email;
	int ID;
	double BMI;
	
	public person() {};
	public person(String p) {
		String delims = "[ ]+";
		String[] tokens;
		tokens = p.split(delims);
		try {
			setHeight(Integer.parseInt(tokens[0]));
			setWeight(Integer.parseInt(tokens[1]));
			setEName(tokens[2]);
			setCName(tokens[3]);
			setExt(Integer.parseInt(tokens[4]));
			setEmail(tokens[5]);
			
			setBMI(Math.round(calBMI(getHeight(),getWeight())));
		}catch(IOException e) {
			System.out.println(e);
		}
		
		
	}  
	
	public void setHeight(int height) throws IOException {
		this.height = height;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public void setWeight(int weight) throws IOException{
		this.weight = weight;
	}
	
	public int getWeight(){
		return this.weight;
	}
	
	public void setEName(String eName) throws IOException {
		this.eName = eName;
	}
	
	public String getEName() {
		return this.eName;
	}
	
	public void setCName(String cName) throws IOException {
		this.cName = cName;
	}
	
	public String getCName() {
		return this.cName;
	}
	
	public void setExt(int ext) throws IOException {
		this.ext = ext;
	}
	
	public int getExt() {
		return this.ext;
	}
	
	public void setBMI(double BMI) throws IOException {
		this.BMI = BMI;
	}
	
	public double getBMI() {
		return this.BMI;
	}
	
	public void setID(int ID) throws IOException {
		this.ID = ID;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public void setEmail(String email) throws IOException {
		this.email = email;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public double calBMI(int height, int weight) throws IOException {
		return (weight / Math.pow((height/100.00), 2));
	}
		
	public void printPerson() {
		System.out.println("Height: " + getHeight());
		System.out.println("Weight: " + getWeight());
		System.out.println("eName: " + getEName());
		System.out.println("cName: " + getCName());
		System.out.println("Ext: " + getExt());
		System.out.println("email: " + getEmail());
		System.out.println("BMI: " + getBMI());
		System.out.println("ID: " + getID());
	}
	
	@Override
	public String toString() {
	    StringBuilder result = new StringBuilder();
	    result.append(this.height + " ");
	    result.append(this.weight + " ");
	    result.append(this.eName + " ");
	    result.append(this.cName + " ");
	    result.append(this.ext + " ");
	    result.append(this.email + " ");
	    result.append(this.ID + " ");
	    result.append(this.BMI + " ");
	    return result.toString();
	}
	
	public static Comparator<person> heightComparator = new Comparator<person>() { 
	    @Override      
	    public int compare(person p1, person p2) { 
	      return (p2.getHeight() > p1.getHeight() ? -1 : 
	              (p2.getHeight() == p1.getHeight() ? 0 : 1)); 
	    }     

	};   
}
