package FileIO;

import java.util.*;

public class personSorting {
	ArrayList<person> p = new ArrayList<>();

	public personSorting(ArrayList<person> p) {
		this.p = p;
	}
	
	public ArrayList<person> getSortedPersonByHeight(){
		Collections.sort(p, person.heightComparator);		
		return p;
	}

}
