package FileIO;

import java.util.*;

public class personSearch {

	public personSearch() {	}
	
	public List<String> searchByHeight(ArrayList<person> p, int s){
		List<String> result = new ArrayList<String>();
		for(int x = 0; x < p.size(); x++) {
			if (p.get(x).getHeight() == s) {
				result.add(p.get(x).toString());
			}
		}
		return result;
	}
	
	public List<String> searchByWeight(ArrayList<person> p, int s){
		List<String> result = new ArrayList<String>();
		for(int x = 0; x < p.size(); x++) {
			if (p.get(x).getWeight() == s) {
				result.add(p.get(x).toString());
			}
		}
		return result;
	}
	
	public List<String> searchByEName(ArrayList<person> p, String s){
		List<String> result = new ArrayList<String>();
		for(int x = 0; x < p.size(); x++) {
			if (p.get(x).getEName() == s) {
				result.add(p.get(x).toString());
			}
		}
		return result;
	}
	
	public List<String> searchByCName(ArrayList<person> p, String s){
		List<String> result = new ArrayList<String>();
		for(int x = 0; x < p.size(); x++) {
			if (p.get(x).getCName() == s) {
				result.add(p.get(x).toString());
			}
		}
		return result;
	}
	
	public List<String> searchByEXT(ArrayList<person> p, int s){
		List<String> result = new ArrayList<String>();
		for(int x = 0; x < p.size(); x++) {
			if (p.get(x).getExt() == s) {
				result.add(p.get(x).toString());
			}
		}
		return result;
	}
	
	public List<String> searchByEmail(ArrayList<person> p, String s){
		List<String> result = new ArrayList<String>();
		for(int x = 0; x < p.size(); x++) {
			if (p.get(x).getEmail() == s) {
				result.add(p.get(x).toString());
			}
		}
		return result;
	}
	
	public List<String> searchByID(ArrayList<person> p, int s){
		List<String> result = new ArrayList<String>();
		for(int x = 0; x < p.size(); x++) {
			if (p.get(x).getID() == s) {
				result.add(p.get(x).toString());
			}
		}	
		return result;
	}
	
	public List<person> searchByBMI(ArrayList<person> p, int s){
		List<person> result = new ArrayList<person>();
		for(int x = 0; x < p.size(); x++) {
			if (Math.round(p.get(x).getBMI()) == s) {
				result.add(p.get(x));
			}
		}	
		return result;
	}
}
